# MS-Balances

MS-Balances is a Node.js microservice for working with accounts balances.

## ENV Variables
MONGODB_URI, PORT (default 3000), HOST (default "0.0.0.0"),
JWT_SECRET

## Usage

```bash
npm start
```