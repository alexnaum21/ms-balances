// External Dependencies
const mongoose = require('mongoose')

const MydRewardsTransactionSchema = new mongoose.Schema({
    type: { type: String},
    reward_points: { type: Number},
    customer_id: { type: mongoose.Schema.Types.ObjectId, ref: 'MydClient' },
  },
  {
    timestamps: true,
    collection: 'mydrewardstransaction'
  })
module.exports = mongoose.model('MydRewardsTransaction', MydRewardsTransactionSchema)
