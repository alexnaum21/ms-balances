// External Dependencies
const mongoose = require('mongoose')

const MydClientSchema = new mongoose.Schema({
  phone: { type: String, required: true},
  wabiBalance: { type: Number},
  openid: { type: String },
  description: { type: String, default: '' },
  isActive: { type: Boolean, default: false },
  onMain: { type: Boolean, default: false }
},
{
  timestamps: true,
  collection: 'mydClients'
})
module.exports = mongoose.model('MydClient', MydClientSchema)
