// External Dependencies
const mongoose = require('mongoose')

const UnionIdToCustomerIdSchema = new mongoose.Schema({
    unionid: { type: String},
    customerMongoId: { type: mongoose.Schema.Types.ObjectId, ref: 'MydClient' },
    },
  {
    timestamps: true,
    collection: 'unionIdToCustomerId'
  })
module.exports = mongoose.model('UnionIdToCustomerId', UnionIdToCustomerIdSchema)
