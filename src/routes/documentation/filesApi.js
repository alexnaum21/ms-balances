exports.addFileSchema = {
  description: 'Upload file',
  tags: ['files'],
  summary: 'Import new xls file',
  body: {
    type: 'file'
  },
  response: {
    200: {
      description: 'Successful response',
      type: 'object',
      properties: {
        stat: { type: 'object' }
      }
    }
  }
}
