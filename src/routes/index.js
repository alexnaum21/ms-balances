// Import our Controllers
const balancesController = require('../controllers/balancesController')

// Import Swagger documentation
// const documentation = require('./documentation/filesv1')

const routes = [

  {
    method: 'GET',
    url: '/api/balance',
    handler: balancesController.getConsumerBalance
  },
];

module.exports = routes
