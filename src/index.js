// Require the fastify framework and instantiate it
const fastify = require('fastify')({
  logger: true
})

const mongoose = require('mongoose')
const fs = require('fs')

const path = require('path')
// Import Routes
const routes = require('./routes')

// Import Swagger Options
const swagger = require('./config/swagger');
const pluginTimeout = require('fastify-server-timeout')

// Register Swagger
fastify
  .register(require('fastify-swagger'), swagger.options)
  .register(require('fastify-auth'))
  .decorate('authJwt', require('./services/authJwt')(fastify))
  .register(require('fastify-jwt'), { secret: process.env.JWT_SECRET || 'supersecret' })
  .after(() => {
    routes.forEach((route, index) => {
      if(route.jwtRequired){

        route.preHandler = fastify.auth([
          fastify.authJwt
        ]);
        fastify.route(route)
      }else fastify.route(route)
    });
  })

fastify.register(require('fastify-static'), {
  root: path.join(__dirname, '../public'),
  prefix: '/public/' // optional: default '/'
});

fastify.register(pluginTimeout, {
  serverTimeout: 30000 //ms
});

fastify.register(require('fastify-cors'), {
  // put your options here
  origin: false
})

mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/walimai_app')
  .then(() => console.log('MongoDB connected...'))
  .catch(err => console.log(err))


fastify.ready(err => {
  if (err) throw err

  console.log('Server started.')

})

// Run the server!
const start = async () => {
  try {
    await fastify.listen(process.env.PORT || 3000, process.env.HOST || '0.0.0.0')
    if (!fs.existsSync('public')) {
      fs.mkdirSync('public', '0777')
    }
 /*   const hH = require('./services/hyperledger');
    await hH.initUser();*/
    fastify.swagger()
    fastify.log.info(`server listening on ${fastify.server.address().port}`)
  } catch (err) {
    fastify.log.error(err)
    console.log(err)
    process.exit(1)
  }
}

start()