// External Dependancies
const boom = require('boom')

// Get Data Models
const MydClient = require('../models/MydClient');
const MydRewardTransaction = require('../models/MydRewardsTransaction');
const reducerBalances = (accumulator, currentValue) => accumulator + currentValue.wabiBalance;

exports.getBalance = async (req, reply) => {
  try {
    let err
    let searchCond = {};
    if(req.query.phone) searchCond.phone = req.query.phone;
    else {
      err = new Error("Phone field missed");
      err.statusCode = 400;
      throw err;
    }

    let existClients = await MydClient.find(searchCond);

    searchCond.balance = existClients.reduce(reducerBalances,0);

    return searchCond;

  } catch (err) {
    throw boom.boomify(err)
  }
}

exports.createBalanceTx = async (req, reply) => {
  try {
    let err
    let searchCond = {};
    if(req.body.phone) searchCond.phone = req.body.phone;
    else {
      err = new Error("Phone field missed");
      err.statusCode = 400;
      throw err;
    }

    let existClient = await MydClient.findOne(searchCond);

    if(!existClient){
      err = new Error("Client not found");
      err.statusCode = 404;
      throw err;
    }

    let rewardObj = {
      type: req.body.type,
      reward_points: req.body.rewardPoints,
      customer_id: existClient._id,
    }
    existClient.wabiBalance += rewardObj.reward_points;
    await existClient.save();
    await MydRewardTransaction.create(rewardObj);

    return existClient.toJSON();

  } catch (err) {
    throw boom.boomify(err)
  }
}
