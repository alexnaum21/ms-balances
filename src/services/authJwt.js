

module.exports = fastify => (req, res, next, access = {}) => {

  if (req.query.token === 'null') {
    delete req.query.token;
  }

  // Public access without token.
  if (!req.query.token && access.public) {
    return next();
  }


  // Verify token (checks token expiry)
  req.jwtVerify( async (err, token) => {
    if (err) {
      fastify.log.error(err);
      return res.code(401).send(new Error('Invalid token.'));
    }

    // Token must have an email
    if (!token.email) {
      return res.code(401).send(new Error('Invalid token.'));
    }


    // Continue if neither admin nor editor previliges are required.
    if (!access.admin_user && !access.admin_workspace) return next();

    // Check admin_user privileges.
    if (access.admin_user && token.admin_user) return next();

    // Check admibn_workspace privileges.
    if (access.admin_workspace && token.admin_workspace) return next();

    res.code(401).send(new Error('Invalid token.'));

  });

};